const express = require('express');
const mongoDb = require('./mongoDb');
const mongoose = require('mongoose');
const links = require('./app/links');
const cors = require("cors");
const exitHook = require('async-exit-hook');


const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

app.use('/link', links);

const port = 8000;


const run = async () => {
    await mongoose.connect('mongodb://localhost/link', {useNewUrlParser: true, useUnifiedTopology: true});

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async (callback) => {
        await mongoose.disconnect();
        console.log('disconnect');
        callback();
    });
};

run().catch(console.error)



