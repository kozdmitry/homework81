const express = require('express');
const path = require('path');
const config = require('../config');
const router = express.Router();
const multer = require('multer');
const Link = require("../models/Link");

const {nanoid} = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});


router.get('/', async (req, res) => {
    try{
        const links = await Link.find();
        res.send(links)
    } catch (e) {
        res.sendStatus(500);
    }
    res.send(links);
});


router.get("/:link", async (req, res) => {
    try {
        const linkInfo = await Link.findOne({ shortUrl: req.params.shortUrl });
        if (linkInfo) {
            res.status(301).redirect(linkInfo.originalUrl);
        } else {
            res.sendStatus(404);
        }
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    try {
        const linksData = req.body;

        const link = new Link({ ...linksData, shortUrl: nanoid(15), _id: nanoid(5) });
        await link.save();
        res.send(link);
    } catch {
        res.sendStatus(500);
    }
});

module.exports = router;