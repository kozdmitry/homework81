import React, {useState} from 'react';
import {Container, CssBaseline, Grid, Paper, TextField, Toolbar} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {createUrl} from "../../store/actions";

const InputUrl = () => {
    const dispatch = useDispatch();
    const links = useSelector((state) => state.links);

    const [data, setData] = useState({
            originalUrl: '',
        }
    );

    const submitFormHandler = (e) => {
        e.preventDefault();
        dispatch(createUrl(data));
        setData({
            originalUrl: '',
        });
    };

    const onChangeHandler = (e) => {
        const name = e.target.name;
        const value = e.target.value;

        setData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    return (
        <>
            <CssBaseline/>
            <Container>
                <Toolbar/>
                <form onSubmit={submitFormHandler}>
                    <Grid container direction="column" spacing={2}>
                        <Grid item xs>
                            <TextField
                                fullWidth
                                required
                                name="originalUrl"
                                label="URL"
                                multiline
                                rows={2}
                                variant="outlined"
                                value={data.originalUrl}
                                onChange={onChangeHandler}
                            >
                            </TextField>
                        </Grid>
                        <Grid item>
                            <button onClick={submitFormHandler}>Send</button>
                        </Grid>
                        <Toolbar/>
                    </Grid>
                </form>
                <Grid item>
                        <Paper>
                            <Grid container direction="column" spacing={2} alignContent={"center"}>
                                <p>Your Link now looks like this:</p>
                                <a href={links.originalUrl}>{'http://localhost:8000/' + links._id}</a>
                            </Grid>
                        </Paper>
                </Grid>
            </Container>
        </>
    );
};

export default InputUrl;