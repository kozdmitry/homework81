import {POST_URL_SUCCESS} from "./actions";

const initialState = {
    links: false,
};

console.log(initialState);
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_URL_SUCCESS:
            return {...state, loading: false, links: action.url};
        default:
            return state;
    }
};

export default reducer;