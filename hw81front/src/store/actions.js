import axiosApi from "../axiosApi";

export const POST_URL_SUCCESS = 'POST_URL_SUCCESS';

export const postUrlSuccess = url => ({type: POST_URL_SUCCESS, url});


export const createUrl = (url) => {
    return async (dispatch) => {
        try{
            const response = await axiosApi.post('/link', url);
            dispatch(postUrlSuccess(response.data))
        } catch (e) {
            console.log(e)
        }
    };
};

